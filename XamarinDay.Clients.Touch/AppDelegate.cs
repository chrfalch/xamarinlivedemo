﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Xamarin.Forms;
using XamarinDay.FormsApp;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using System.IO;

namespace XamarinDay.Clients.Touch
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		UIWindow window;

		//
		// This method is invoked when the application has loaded and is ready to run. In this
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			// create a new window instance based on the screen size
			window = new UIWindow (UIScreen.MainScreen.Bounds);

			Forms.Init ();

			// If you have defined a root view controller, set it here:
			window.RootViewController = App.GetMainPage(CreateLocalStore()).CreateViewController();
			
			// make the window visible
			window.MakeKeyAndVisible ();
			
			return true;
		}

		private MobileServiceSQLiteStore CreateLocalStore()
		{
			var path = Path.Combine (System.Environment.GetFolderPath(Environment.SpecialFolder.Personal), "contacts.db");

			if (!File.Exists (path))
				File.Create (path).Dispose ();

			Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init ();
			SQLitePCL.CurrentPlatform.Init ();

			return new MobileServiceSQLiteStore (path);
		}
	}
}

