﻿using System;
using Xamarin.Forms;
using XamarinDay.DataLayer;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices;

namespace XamarinDay.FormsApp
{
	public class App
	{
		public static SyncableRepository<Contact> ContactRepository;

		public static Page GetMainPage (MobileServiceSQLiteStore localStore)
		{	
			var client = new MobileServiceClient ("https://msxamarinday.azure-mobile.net/",
				"GkvAzpaNWhtRFRWKAUWsNzrIGlyCts43");

			ContactRepository = new SyncableRepository<Contact> (client, localStore);
			localStore.DefineTable<Contact> ();

			return new NavigationPage (new ContactsView ());
		}
	}
}

