﻿using System;
using Xamarin.Forms;

namespace XamarinDay.FormsApp
{
	public class ContactsView: BaseView<ContactsViewModel>
	{
		public ContactsView ()
		{
			var textField = new Entry {
				HorizontalOptions = LayoutOptions.FillAndExpand
			};
			textField.SetBinding (Entry.TextProperty, "NewContactName");
			var button = new Button {
				Text = "+"
			};
			button.Command = ViewModel.AddNewCommand;

			var headerLayout = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {textField, button}
			};

			var listView = new ListView {
				RowHeight = 55
			};

			listView.ItemsSource = ViewModel.Contacts;
			listView.ItemTemplate = new DataTemplate (typeof(TextCell));
			listView.ItemTemplate.SetBinding (TextCell.TextProperty, "Name");

			Content = new StackLayout{
				Orientation = StackOrientation.Vertical,
				Children = {headerLayout, listView}
			};
		}
	}
}

