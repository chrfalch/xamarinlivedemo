﻿using System;
using System.Collections.ObjectModel;
using XamarinDay.DataLayer;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace XamarinDay.FormsApp
{
	public class ContactsViewModel: BaseViewModel
	{
		private readonly ObservableCollection<Contact> _contacts = 
			new ObservableCollection<Contact>();

		public ObservableCollection<Contact> Contacts { get { return _contacts; } }

		public override void OnAppearing ()
		{
			base.OnAppearing ();

			RefreshCommand.Execute (null);
		}

		private string _newContactName;
		public string NewContactName {
			get{
				return _newContactName;
			}
			set{
				if (_newContactName == value)
					return;

				_newContactName = value;

				NotifyPropertyChanged (() => NewContactName);
			}
		}

		public Command AddNewCommand
		{
			get{
				return new Command (async () => {

					await App.ContactRepository.AddAsync(new Contact{
						Name = NewContactName
					});

					RefreshCommand.Execute(null);

				});
			}
		}

		public Command RefreshCommand {
			get{
				return new Command ((param) => {

					_contacts.Clear();

					Task.Run(async () =>{

						await App.ContactRepository.RefreshDataAsync();

						var items = await App.ContactRepository.GetItemsAsync();

						Device.BeginInvokeOnMainThread(() =>{

							// Update on UI thread
							foreach(var item in items)
								_contacts.Add(item);

						});

					});
				});
			}
		}
	}
}

