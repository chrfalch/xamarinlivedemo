﻿using System;
using System.Linq.Expressions;
using System.ComponentModel;

namespace XamarinDay.FormsApp
{
	public class BaseViewModel: INotifyPropertyChanged
	{
		#region INotifyPropertyChanged implementation

		/// <summary>
		/// Occurs when property changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor
		/// </summary>
		public BaseViewModel ()
		{
		}

		#endregion

		#region ViewModel LifeCycle

		/// <summary>
		/// Override to implement logic when the view has been set up on screen
		/// </summary>
		public virtual void OnAppearing()
		{

		}

		#endregion

		#region Properties

		/// <summary>
		/// Returns the view title
		/// </summary>
		/// <value>The view title.</value>
		public virtual string ViewTitle { get{ return string.Empty;}}

		#endregion

		#region Protected Members

		/// <summary>
		/// Calls the notify property changed event if it is attached. By using some
		/// Expression/Func magic we get compile time type checking on our property
		/// names by using this method instead of calling the event with a string in code.
		/// </summary>
		/// <param name="property">Property.</param>
		protected void NotifyPropertyChanged(Expression<Func<object>> property)
		{
			if (PropertyChanged == null)
				return;

			string propertyName = string.Empty;

			if (property.Body.NodeType == ExpressionType.MemberAccess)
			{
				var memberExpression = property.Body as MemberExpression;
				if (memberExpression != null)
					propertyName = memberExpression.Member.Name;
			}
			else
			{
				var unary = property.Body as UnaryExpression;
				if (unary != null)
				{
					var member = unary.Operand as MemberExpression;
					if (member != null) propertyName = member.Member.Name;
				}
			}

			PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
		}

		#endregion
	}
}

