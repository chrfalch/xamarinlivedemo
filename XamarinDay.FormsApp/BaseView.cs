﻿using System;
using Xamarin.Forms;
using System.Linq.Expressions;

namespace XamarinDay.FormsApp
{
	public class BaseView<TViewModel>: ContentPage
		where TViewModel : BaseViewModel, new()
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="com.xamarinmeetup.crm.app.BaseView`1"/> class.
		/// </summary>
		public BaseView ()
		{
			ViewModel = new TViewModel ();
			BindingContext = ViewModel;
			Title = ViewModel.ViewTitle;
		}

		#endregion

		#region Protected Propertyes

		/// <summary>
		/// The view model.
		/// </summary>
		protected TViewModel ViewModel { get; set;}

		#endregion

		#region View LifeCycle

		/// <summary>
		/// Raised when the view has appeared on screen.
		/// </summary>
		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			ViewModel.OnAppearing ();
		}

		#endregion
	}
}

