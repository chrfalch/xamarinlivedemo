﻿using System;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Sync;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace XamarinDay.DataLayer
{
	public class SyncableRepository<TModel>
	{
		private readonly IMobileServiceClient _client;
		private readonly IMobileServiceLocalStore _localStore;
		private IMobileServiceSyncTable<TModel> _table;

		public SyncableRepository (IMobileServiceClient client, IMobileServiceLocalStore localStore)
		{
			_client = client;
			_localStore = localStore;
		}

		public async Task RefreshDataAsync()
		{
			await InitializeAsync ();

			await _client.SyncContext.PushAsync ();
			await _table.PullAsync ();
		}

		public async Task AddAsync(TModel entity)
		{
			await InitializeAsync ();

			await _table.InsertAsync (entity);
		}

		public async Task<IEnumerable<TModel>> GetItemsAsync()
		{
			await InitializeAsync ();

			return await _table.ToEnumerableAsync ();
		}

		private bool _isInitialized = false;
		private async Task InitializeAsync()
		{
			if (_isInitialized)
				return;

			await _client.SyncContext.InitializeAsync (_localStore);

			_table = _client.GetSyncTable<TModel> ();

			_isInitialized = true;
		}
	}
}

