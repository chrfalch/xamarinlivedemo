﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using XamarinDay.FormsApp;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using System.IO;

namespace XamarinDay.Clients.Android
{
	[Activity (Label = "XamarinDay.Clients.Android", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : AndroidActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Forms.Init (this, bundle);

			SetPage (App.GetMainPage (CreateLocalStore()));
		}

		private MobileServiceSQLiteStore CreateLocalStore()
		{
			var path = Path.Combine (System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "contacts.db");

			if (!File.Exists (path))
				File.Create (path).Dispose ();

			Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init ();
			//SQLitePCL.CurrentPlatform.Init ();

			return new MobileServiceSQLiteStore (path);
		}
	}
}


